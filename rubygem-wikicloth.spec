%global gem_name wikicloth

Name: rubygem-%{gem_name}
Version: 0.8.0
Release: 1%{?dist}
Summary: Mediawiki parser
Group: Development/Languages
License: MIT
URL: https://github.com/nricciar/wikicloth
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems)
Requires: rubygem(builder)
Requires: rubygem(expression_parser)
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: rubygem(activesupport)
BuildRequires: rubygem(builder)
BuildRequires: rubygem(expression_parser)
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
An implementation of the mediawiki markup in Ruby.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

# Remove the old-style loading for test-unit
sed -i -e '/gem "test-unit"/d' test/test_helper.rb

# Remove developer-only files.
for f in .gitignore .travis.yml Gemfile Rakefile run_tests.rb tasks/wikicloth_tasks.rake; do
  rm $f
  sed -i "s|\"$f\",||g" %{gem_name}.gemspec
done

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

%gem_install

# Remove unnecessary gemspec
rm .%{gem_instdir}/%{gem_name}.gemspec

%install
mkdir -p %{buildroot}%{gem_dir}
cp -pa .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%check
pushd .%{gem_instdir}
  ruby -Ilib test/*_test.rb
popd


%files
%dir %{gem_instdir}
%doc %{gem_instdir}/README
%doc %{gem_instdir}/README.textile
%doc %{gem_instdir}/MIT-LICENSE
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}
%{gem_instdir}/init.rb
%{gem_instdir}/lang

%files doc
%doc %{gem_docdir}
%exclude %{gem_instdir}/test
%{gem_instdir}/examples
%{gem_instdir}/sample_documents

%changelog
* Wed Nov 06 2013 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.8.0-1
- Initial package
